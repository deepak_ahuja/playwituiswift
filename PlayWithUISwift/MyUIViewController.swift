//
//  MyUIViewController.swift
//  PlayWithUISwift
//
//  Created by Sthuthi Shetty on 30/05/16.
//  Copyright © 2016 Sthuthi Shetty. All rights reserved.
//

import Foundation
import UIKit

class MyUIViewController: UIViewController {
    @IBOutlet weak var myLabel: UILabel!
    var initialLocationOfLabel:CGPoint!

    override func viewDidLoad() {
        myLabel.userInteractionEnabled = true
        initialLocationOfLabel = myLabel.center
        let panGesture:UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action:#selector(MyUIViewController.myLabelDragged(_:)))
        myLabel.addGestureRecognizer(panGesture)
    }
    
    func myLabelDragged(gesture:UIPanGestureRecognizer)->Void  {
        let translation = gesture.translationInView(view);
        gesture.view!.center = CGPoint(x: gesture.view!.center.x + translation.x, y: gesture.view!.center.y + translation.y);
        gesture.setTranslation(CGPointZero, inView: view);

        if gesture.state == UIGestureRecognizerState.Ended {
            myLabel.center = initialLocationOfLabel
        }
    }
}