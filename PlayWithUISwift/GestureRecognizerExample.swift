class ViewController: UIViewController {

    @IBOutlet weak var square: UIView!
    
    var squareMidpoint:CGPoint = CGPointZero
    
    @IBAction func handlePan(recognizer: UIPanGestureRecognizer) {
        let hitInRect = CGRectContainsPoint(square.frame, recognizer.locationInView(self.view))
        let isMoving  = (squareMidpoint != CGPointZero)
        
        switch(recognizer.state) {
        case .Began where hitInRect:
            squareMidpoint = square.center
        case .Changed where isMoving:
            let translation = recognizer.translationInView(self.view)
            let newX        = squareMidpoint.x + translation.x
            let newY        = squareMidpoint.y + translation.y
            square.center   = CGPoint(x: newX, y: newY)
        default:
            squareMidpoint = CGPointZero
        }
    }
}